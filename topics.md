# Organized topics

##### This is meant to be markdown compatible. If developing bots using this topics list make them ignore anything starting with a `#` and empty lines (i.e. replacing `\n\n` with `\n`).

##### Last update: 19 September 2024.

## General on-topic questions

the pros of Pixel phones.

the future of Android.

how better or worse Android would be had Google not bought it.

the cons of Pixel phones.

## Android versions

Android 12.

Android 13.

Android 14.

Android 15.

### Skin versions

ColorOS 13.

OxygenOS 13.

EMUI 14.

MIUI 14.

MIUI 15.

MIUI 13.

HyperOS.

One UI 4.

One UI 5.

One UI 6.

One UI 7.

## Aftermarket and privacy

open-source software.

custom ROMs.

degoogling.

F-Droid.

the GNU project.

Linux on phones.

## Desktop OSes

ChromeOS.

Windows.

Linux.

macOS.

Ubuntu.

Arch Linux.

Linux Mint.

Gentoo.

## Computers

MacBooks.

Windows laptops.

Chromebooks.

## Android skins and mobile software

ColorOS.

OxygenOS.

LineageOS.

HarmonyOS.

MIUI.

One UI.

Google software.

Nothing OS.

realme UI.

## Phones

the Nothing phone series.

the Nothing ear series.

the Pixel 6a.

the Pixel 7a.

the Pixel 7 series.

the Pixel 8 series.

the Pixel 9 series.

the Pixel 6 series.

the Pixel Watch.

the Pixel Buds A-series.

the Pixel Buds Pro.

the Pixel Fold series.

the Galaxy Z lineup.

the Galaxy Z Flip6.

the Galaxy Z Fold6.

the Galaxy S24 series.

the iPhone 14 series.

the iPhone 16 series.

the iPhone 15 series.

the Xiaomi 12s.

the Xiaomi 12T.

the Xiaomi 13.

a potential cheaper Galaxy foldable (A Flip? Z Flip FE?).

## OEMs

Xiaomi.

Huawei.

Nothing (the tech company).

BBK.

Samsung.

## SoCs and SoC vendors

Qualcomm.

Exynos.

Google Tensor.

the M1 Pro and M1 Max.

the M2.

the M2 Pro.

Intel.

Unisoc.

AMD.

Kirin.

TSMC.

Samsung Foundry.

the Ryzen CPU lineup.

## AI

OpenAI.

OpenAI o1.

GPT-4.

ChatGPT.

Google Gemini.

Meta Llama.

Google Gemma.

large language models (LLMs).

Stable Diffusion.

Midjourney.

## Other

Google Stadia.

Telegram Premium.

Nvidia.

the RTX 20 series.

the RTX 30 series.

the RTX 40 series.

GeForce NOW.

the future of cloud gaming.

the Steam Deck.
